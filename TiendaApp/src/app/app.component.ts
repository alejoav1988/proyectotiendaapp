import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages: any=[
    {
      Titulo:"Inicio",Ruta:"inicio",Icono:"american-football"
    },
  {
    Titulo:"Categorías",Ruta:"categoria",Icono:"american-football"
  }
  ,
  {
    Titulo:"Usuarios",Ruta:"usuario",Icono:"airplane"
  }
  ,
  {
    Titulo:"Productos",Ruta:"#",Icono:"airplane"
  }
  ,
  {
    Titulo:"Compra",Ruta:"#",Icono:"airplane"
  }
  ,
  {
    Titulo:"Administración",Ruta:"#",Icono:"airplane"
  }
]

  public labels = ['Minecraf', 'LOL', 'Call of Dutty', 'Halo', 'CupHead', 'Reminders'];
  constructor() {}
}
