import { Component, OnInit } from '@angular/core';
//import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.page.html',
  styleUrls: ['./categoria.page.scss'],
})
export class CategoriaPage implements OnInit {

  formularioActivo = this.fb.group({
    nombreCategoria: ['',Validators.required],
    descripcionCategoria: ['',Validators.required],
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
  }

  guardar()
  {
    console.log(this.formularioActivo);
  }
}
