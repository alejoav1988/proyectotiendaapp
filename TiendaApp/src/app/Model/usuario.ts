export interface Usuario {
    id: number;
    tipoIdentificacion: number;
    numeroIdentificacion: string;
    nombre: string;
    email: string;
    telefono: string;
    celular: string;
    direccion: string;
    imagen: string;
    fechaDeNacimiento: Date;
    codigoUsuario: string;
    fechaIngreso: Date;
    fechaRetiro: Date;
    estado: number;
    ciudad: string;
    genero: number;
    tipoUsuario: number;

}
