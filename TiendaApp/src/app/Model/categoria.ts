export interface Categoria {
    id: number;
    nombreCategoria: string;
    descripcion: string;
}
