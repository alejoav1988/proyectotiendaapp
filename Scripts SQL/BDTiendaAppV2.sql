create table Categoria_Alejandro_1060
(
id int auto_increment primary key,/*numerico*/
Nombre_Categoria nvarchar(100),/*Texto*/
Descripcion nvarchar(500),
nuevocampo int
)
;
Create table Producto_Alejandro_1060
(
id int auto_increment primary key,
Codigo_Producto_Servicio nvarchar(4), 
NombreProductoServicio nvarchar(100),
DescripcionProductoServicio nvarchar(2000),
ImagenPrincipal nvarchar(200),
ID_Categoria int
)
;
alter table Producto_Alejandro_1060 add foreign key (ID_Categoria) references Categoria_Alejandro_1060 (id)
;
create table Tipodocumento_alejandro_1060
(
id int primary key auto_increment,
TipoDocumento nvarchar(100) not null,
Descripcion nvarchar(500)
)
;
create table TipoUsuario_alejandro_1060
(
id int primary key auto_increment,
TipoUsuario nvarchar(100) not null,
Descripcion nvarchar(500)
)
;
create table Ciudad_alejandro_1060
(
id int primary key auto_increment,
NombreCiudad nvarchar(100) not null,
Departamento nvarchar(500)
)
;
create table Genero_alejandro_1060
(
id int primary key auto_increment,
Genero nvarchar(100) not null,
Descripcion nvarchar(500)
)
;
create table Personas_alejandro_1060
(
id int primary key auto_increment,
TipoIdentificacion int not null,
NumeroIdentificacion nvarchar(100),
Nombre nvarchar(100),
Email nvarchar(100),
Telefono nvarchar(100),
Celular nvarchar(100),
Direccion nvarchar(100),
Imagen nvarchar(100),
/*DAtos Vendedor,Cliente y proveedor   */
FechaDeNacimiento date,
CodigoUsuario nvarchar(10),
FechaIngreso date,
FechaRetiro date,
Estado int,
Ciudad int,
Genero int,
TipoUsuario int,
foreign key (TipoIdentificacion) references Tipodocumento_alejandro_1060(id),
foreign key (Ciudad) references Ciudad_alejandro_1060(id),
foreign key (Genero) references Genero_alejandro_1060(id),
foreign key (TipoUsuario) references TipoUsuario_alejandro_1060(id)
)
;
create table Compra_Alejandro_1060
(
Id int primary key auto_increment,
NumeroFactura nvarchar(100),
FechaGeneracion datetime,
Observacion nvarchar(500),
Persona int,
foreign key (Persona) references Personas_alejandro_1060(id)
)
;
create table DetalleCompra_alejandro_1060 
(
Id int primary key auto_increment, 
IdProducto int not null,
IdCompra int not null,
Precio float,
Cantidad int,
foreign key (IdProducto) references Producto_Alejandro_1060(id),
foreign key (IdCompra) references Compra_Alejandro_1060(id)
)